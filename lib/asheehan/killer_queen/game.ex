defmodule Asheehan.KillerQueen.Game do
  use Ecto.Schema
  import Ecto.Changeset

  schema "games" do
    field :map, :string
    field :victor, :string
    field :victory_type, :string
    field :blue_queen_id, :id
    field :blue_checks_id, :id
    field :blue_skulls_id, :id
    field :blue_abs_id, :id
    field :blue_stripes_id, :id
    field :gold_queen_id, :id
    field :gold_checks_id, :id
    field :gold_skulls_id, :id
    field :gold_abs_id, :id
    field :gold_stripes_id, :id

    timestamps()
  end

  @doc false
  def changeset(game, attrs) do
    game
    |> cast(attrs, [
      :map,
      :victor,
      :victory_type,
      :blue_queen_id,
      :blue_checks_id,
      :blue_skulls_id,
      :blue_abs_id,
      :blue_stripes_id,
      :gold_queen_id,
      :gold_checks_id,
      :gold_skulls_id,
      :gold_abs_id,
      :gold_stripes_id
    ])
    |> validate_required([:map, :victor, :victory_type])
  end
end
