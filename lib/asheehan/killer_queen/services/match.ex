defmodule Asheehan.KillerQueen.Service.Match do
  use GenServer

  alias Asheehan.KillerQueen.MatchState

  def start_link(default) when is_list(default) do
    GenServer.start_link(__MODULE__, default, name: __MODULE__)
  end

  def create(players) when is_list(players) do
    GenServer.call(__MODULE__, {:set_players, Enum.shuffle(players)})
  end

  def set_players(players) when is_list(players) do
    GenServer.call(__MODULE__, {:set_players, players})
  end

  def get_players do
    GenServer.call(__MODULE__, :get_players)
  end

  def shuffle do
    GenServer.call(__MODULE__, :shuffle)
  end

  @impl true
  def init(_) do
    {:ok, %MatchState{}}
  end

  @impl true
  def handle_call(:shuffle, _from, state) do
    players = Enum.shuffle(state.players)
    {:reply, players, %{state | players: players}}
  end

  @impl true
  def handle_call({:set_players, players}, _from, state) do
    {:reply, players, %{state | players: players}}
  end

  @impl true
  def handle_call(:get_players, _from, state) do
    {:reply, state.players, state}
  end
end
