defmodule Asheehan.KillerQueen.Stats do
  import Ecto.Query, warn: false

  alias Asheehan.KillerQueen.Game
  alias Asheehan.Repo

  def get_stats(player_id) do
    games = played_games(player_id) |> length
    queen_games = queen_games(player_id) |> length
    wins = won_games(player_id) |> length
    queen_wins = queen_wins(player_id) |> length

    %{
      games: games,
      queen_games: queen_games,
      wins: wins,
      queen_wins: queen_wins,
      total_win_percent: win_percent(wins, games),
      queen_win_percent: win_percent(queen_wins, queen_games)
    }
  end

  def win_percent(_, 0), do: nil
  def win_percent(wins, games), do: (wins / games * 100) |> round()

  def won_games(player_id) do
    Game
    |> won_games(player_id)
    |> Repo.all()
  end

  def won_games(query, player_id) do
    blue_wins = query |> blue_won() |> is_blue(player_id)
    gold_wins = query |> gold_won() |> is_gold(player_id)

    union_all(blue_wins, ^gold_wins)
  end

  def played_games(player_id) do
    Game
    |> is_player(player_id)
    |> Repo.all()
  end

  def queen_games(player_id) do
    Game
    |> is_queen(player_id)
    |> Repo.all()
  end

  def queen_wins(player_id) do
    Game
    |> is_queen(player_id)
    |> won_games(player_id)
    |> Repo.all()
  end

  def is_queen(query, player_id) do
    from g in query, where: g.blue_queen_id == ^player_id or g.gold_queen_id == ^player_id
  end

  def is_player(query, player_id) do
    from g in query,
      where:
        g.blue_queen_id == ^player_id or g.gold_queen_id == ^player_id or
          g.blue_abs_id == ^player_id or g.blue_checks_id == ^player_id or
          g.blue_skulls_id == ^player_id or g.blue_stripes_id == ^player_id or
          g.gold_abs_id == ^player_id or g.gold_checks_id == ^player_id or
          g.gold_skulls_id == ^player_id or g.gold_stripes_id == ^player_id
  end

  def blue_won(query), do: from(g in query, where: g.victor == "blue")
  def gold_won(query), do: from(g in query, where: g.victor == "gold")

  def is_blue(query, player_id) do
    from g in query,
      where:
        g.blue_queen_id == ^player_id or g.blue_abs_id == ^player_id or
          g.blue_checks_id == ^player_id or g.blue_skulls_id == ^player_id or
          g.blue_stripes_id == ^player_id
  end

  def is_gold(query, player_id) do
    from g in query,
      where:
        g.gold_queen_id == ^player_id or g.gold_abs_id == ^player_id or
          g.gold_checks_id == ^player_id or g.gold_skulls_id == ^player_id or
          g.gold_stripes_id == ^player_id
  end
end
