defmodule AsheehanWeb.KillerQueen.MatchController do
  use AsheehanWeb, :controller

  alias Asheehan.KillerQueen
  alias Asheehan.KillerQueen.Service.Match

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def new(conn, _params) do
    render(conn, "new.html", players: KillerQueen.player_map())
  end

  def create(conn, %{"players" => players}) do
    players
    |> Enum.map(&String.to_integer/1)
    |> Match.create()

    redirect(conn, to: Routes.game_path(conn, :new))
  end
end
