defmodule AsheehanWeb.PageController do
  use AsheehanWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
