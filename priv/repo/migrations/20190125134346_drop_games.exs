defmodule Asheehan.Repo.Migrations.DropGames do
  use Ecto.Migration

  def up, do: drop_if_exists(table(:games))

  def down, do: :ok
end
