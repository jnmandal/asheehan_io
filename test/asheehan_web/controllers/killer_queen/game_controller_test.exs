defmodule AsheehanWeb.KillerQueen.GameControllerTest do
  use AsheehanWeb.ConnCase

  alias Asheehan.KillerQueen

  @create_attrs %{
    map: "some map",
    victor: "some victor",
    victory_type: "some victory_type"
  }
  @update_attrs %{
    map: "some updated map",
    victor: "some updated victor",
    victory_type: "some updated victory_type"
  }
  @invalid_attrs %{map: nil, recorded_at: nil, victor: nil, victory_type: nil}

  def fixture(:game) do
    {:ok, game} = KillerQueen.create_game(@create_attrs)
    game
  end

  describe "index" do
    test "lists all games", %{conn: conn} do
      conn = get(conn, Routes.game_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Games"
    end
  end

  describe "new game" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.game_path(conn, :new))
      assert html_response(conn, 200) =~ "Blue Team"
    end

    test "accepts players as optional params and renders", c do
      conn = get(c.conn, Routes.game_path(c.conn, :new))
      assert html_response(conn, :ok) =~ "Blue Team"
    end
  end

  describe "shuffle players" do
    test "redirects to new game", c do
      conn = get(c.conn, Routes.game_path(c.conn, :new, shuffle: true))
      assert redirected_to(conn) === Routes.game_path(conn, :new)
    end
  end

  describe "create game" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.game_path(conn, :create), game: @create_attrs)

      assert redirected_to(conn) == Routes.game_path(conn, :new)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.game_path(conn, :create), game: @invalid_attrs)
      assert html_response(conn, 200)
    end
  end

  describe "edit game" do
    setup [:create_game]

    test "renders form for editing chosen game", %{conn: conn, game: game} do
      conn = get(conn, Routes.game_path(conn, :edit, game))
      assert html_response(conn, 200) =~ "Edit Game"
    end
  end

  describe "update game" do
    setup [:create_game]

    test "redirects when data is valid", %{conn: conn, game: game} do
      conn = put(conn, Routes.game_path(conn, :update, game), game: @update_attrs)
      assert redirected_to(conn) == Routes.game_path(conn, :show, game)

      conn = get(conn, Routes.game_path(conn, :show, game))
      assert html_response(conn, 200) =~ "some updated map"
    end

    test "renders errors when data is invalid", %{conn: conn, game: game} do
      conn = put(conn, Routes.game_path(conn, :update, game), game: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Game"
    end
  end

  describe "delete game" do
    setup [:create_game]

    test "deletes chosen game", %{conn: conn, game: game} do
      conn = delete(conn, Routes.game_path(conn, :delete, game))
      assert redirected_to(conn) == Routes.game_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.game_path(conn, :show, game))
      end
    end
  end

  defp create_game(_) do
    game = fixture(:game)
    {:ok, game: game}
  end
end
